from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from.models import Urls
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from django.template import loader
from urllib.parse import urlparse
# Create your views here.

def clear_url(url):
	url = url.replace("+", "")
	matches = ["http://", "https://"]
	if not any([x in url for x in matches]):
		url = "".join(("https://", url))
	return url
	

def get_all_urls():

	# obtener el contendio del template
	content_list = Urls.objects.all()
	# cargar el template
	template = loader.get_template("acorta/index.html")
	# crear el contexto
	contexto = {
		"contenido_lista": content_list
	}
	# Renderizar el template y responder
	return template.render(contexto)

def get_response(welcome):
	template = loader.get_template("acorta/response.html")
	contexto = {
	"welcome": welcome,
	"all_urls": get_all_urls()
	}
	return template.render(contexto)
def no_alias():
	i = 1
	urls = Urls.objects.all()
	while any(url.short_url == str(i) for url in urls):
		i += 1
	alias = str(i)
	return alias

def get_short_url(request):

	short = request.POST['short']
	short = short.replace(" ", "")
	if short == "":
		short = no_alias()
	return short


@csrf_exempt
def welcome(request):

	# if request.method == "PUT":
	# 	try:
	# 		val = request.body.decode()
	# 		contenido = Urls.objects.get(url=url)
	# 		contenido.short_url = val
	# 		contenido.save()
	# 	except Urls.DoesNotExist:
	# 		c = Urls(url=url, short_url=val)
	# 		c.save()

	if request.method == "POST":
		url = request.POST['url']
		url = clear_url(url)
		try:
			c = Urls.objects.get(url=url)
		except Urls.DoesNotExist:
			short = get_short_url(request)
			# short = request.POST['short']
			c = Urls(url=url, short_url=short)
			c.save()

	# html = f"<h1> Bienvenido a la recortadora de urls</h1>"\
	# 	   f"{FORM}"\
	# 	   f"<div>{get_all_urls()}</div>"
	html = get_response(welcome=True)
	return HttpResponse(html)

@csrf_exempt
def get_resource(request, url):
	# Metodos get

	try:
		# Buscar la URL completa asociada al short_url recibido
		resource = Urls.objects.get(short_url=url)
		# Redirigir al usuario a la URL completa
		return redirect(resource.url)

	except Urls.DoesNotExist:
		# Si no se encuentra un registro para el short_url, mostrar un mensaje al usuario
		html = get_response(welcome=False)
		return HttpResponseNotFound(html)