from django.urls import path
from . import views

urlpatterns = [
    # path('', views.index),
    path('', views.welcome),
    path('<str:url>/', views.get_resource),

]